/*
Mapa comparativa de fotos modernas y antiguas
*/

// 8/42.035/-1.769 -- todas
var center = [42.035, -1.769];

// CONFIGURACIÓN DE LAS CAPAS -----------------------------------------

// capa Ortofoto reciente

var layer_PNOA_reciente = L.WMS.layer("https://www.ign.es/wms-inspire/pnoa-ma", "OI.OrthoimageCoverage", {
    //pane: 'pane_OrtofotoPNOA202021_4',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: '© Ortofotografía reciente (2020) del <a href="http://pnoa.ign.es">Plan Nacional de Ortofotografía Aérea (IGN)</a>',
});

// capa 2 - Ortofoto vuelo americano

var layer_vuelo_americano = L.WMS.layer("http://www.ign.es/wms/pnoa-historico?", "AMS_1956-1957", {
    //pane: 'pane_OrtofotoVueloAmericano195657_5',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: '© Ortofotografía Vuelo Americano 1957, fuente: <a href="http://pnoa.ign.es">Plan Nacional de Ortofotografía Aérea (IGN)</a>',
});


/* var layer2 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors',
}); */

// Marcadores de las localidades

var locations = [
['Serué', 42.3729535, -0.4282751],
['Aineto', 42.3924419, -0.1924793],
['Secorún', 42.3994604, -0.1477851],
['Campillo de Ranas', 41.0855605, -3.3141991],
['Matallana', 41.0564798, -3.3358491],
['La Vereda', 41.020976, -3.3443696],
['Villanueva de Arce / Heriberri-Artzibar', 42.9300958, -1.3722077],
['Lacabe', 42.8725157, -1.3456024],
['Gurpegui', 42.8557925, -1.4192607],
];

// CONFIGURACIÓN DE LOS MAPAS -----------------------------------------

var map1 = L.map('map1', {
    layers: [layer_PNOA_reciente],
    center: center,
    zoom: 8
});


// MAPA 1
map1.attributionControl.setPrefix(
    'RENURSE - CC By-SA M. Sevilla-Callejo 20230326 | <a href="https://leafletjs.com" title="A JS library for interactive maps">Leaflet</a>'
    );

/* for (var i = 0; i < locations.length; i++) {
marker = new L.marker([locations[i][1], locations[i][2]])
.bindPopup(locations[i][0])
.addTo(map1);
} */

for (var i = 0; i < locations.length; i++) {
    marker = new L.circleMarker([locations[i][1], locations[i][2]],{
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 5
    })
    .bindPopup(locations[i][0])
    .addTo(map1);
    }

    // escala
L.control.scale(
    {position: 'bottomleft', maxWidth: 100, metric: true, imperial: false, updateWhenIdle: false}).addTo(map1);


// MAPA 2

var map2 = L.map('map2', {
    layers: [layer_vuelo_americano],
    center: center,
    zoom: 8,
    zoomControl: false
});

map2.attributionControl.setPrefix(
    'RENURSE - CC By-SA M. Sevilla-Callejo 20230326 | <a href="https://leafletjs.com" title="A JS library for interactive maps">Leaflet</a>'
    );

for (var i = 0; i < locations.length; i++) {
marker = new L.circleMarker([locations[i][1], locations[i][2]],{
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 5
})
.bindPopup(locations[i][0])
.addTo(map2);
}
    // escala
L.control.scale(
    {position: 'bottomleft', maxWidth: 100, metric: true, imperial: false, updateWhenIdle: false}).addTo(map2);

// SYNC

map1.sync(map2);
map2.sync(map1);


// NOTONES POSICIÓN

    // define botones
L.Control.zoomPosition = L.Control.extend({
    options: {
      position: 'bottomleft'
    },
  
    onAdd: function(map) {
      var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control');
  
      // Crea el botón de Navarra - 12/42.8906/-1.3733
      var sfButton = L.DomUtil.create('button', 'my-zoom-button', container);
      sfButton.title = 'Navarra';
      sfButton.innerHTML = 'NA';
      L.DomEvent.addListener(sfButton, 'click', function() {
        map.setView([42.8906, -1.3733], 12);
      }, this);
  
      // Crea el botón de Guadalajara - 12/41.0539/-3.3271
      var nyButton = L.DomUtil.create('button', 'my-zoom-button', container);
      nyButton.title = 'Guadalajara';
      nyButton.innerHTML = 'GU';
      L.DomEvent.addListener(nyButton, 'click', function() {
        map.setView([41.0539, -3.3271], 12);
      }, this);
  
      // Crea el botón de Huesca - 12/42.3911/-0.2867
      var nyButton = L.DomUtil.create('button', 'my-zoom-button', container);
      nyButton.title = 'Huesca';
      nyButton.innerHTML = 'HU';
      L.DomEvent.addListener(nyButton, 'click', function() {
        map.setView([42.3911, -0.2867], 12);
      }, this);
  
      // Crea el botón de restablecer la vista original - 8/41.983/-1.746
      var resetButton = L.DomUtil.create('button', 'my-zoom-button', container);
      resetButton.title = 'Restablecer vista original';
      resetButton.innerHTML = 'Reset';
      L.DomEvent.addListener(resetButton, 'click', function() {
        map.setView([41.983, -1.746],8);
      }, this);
  
      return container;
    }
  });

    // añade el control de los botones

    // al mapa1
L.control.zoomPosition = function(opts) {
    return new L.Control.zoomPosition(opts);
    }
    
    L.control.zoomPosition({
    position: 'topright'}).addTo(map1);

    // al mapa 2
L.control.zoomPosition = function(opts) {
    return new L.Control.zoomPosition(opts);
    }
    
    L.control.zoomPosition({
    position: 'topright'}).addTo(map2);


